
import './App.css';
import { increment,decrement } from './actions/index';
import {useSelector,useDispatch} from 'react-redux';
function App() {

  const state = useSelector(state => state.alterNum)
  const dispatch=useDispatch();
  return (
    <div className="App">
      <input value={state} />
      <button onClick={()=>dispatch(decrement())}>Sub</button>
      <button onClick={()=>dispatch(increment())}>Add</button>
    </div>
  );
}

export default App;
